package no.uib.inf101.gridview;

import javax.swing.JFrame;
import javax.swing.*;
import java.awt.Color;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    

    ColorGrid colorGrid = new ColorGrid(3, 4);
    colorGrid.set(new CellPosition(0, 0), Color.RED);
    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
    colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
    colorGrid.set(new CellPosition(2, 3), Color.GREEN);

    GridView view = new GridView(null);
    JFrame frame = new JFrame();
    frame.setContentPane(view);

    frame.setTitle("Lab4");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);


  }
}
