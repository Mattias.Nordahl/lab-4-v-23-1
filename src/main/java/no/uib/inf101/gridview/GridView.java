package no.uib.inf101.gridview;


import no.uib.inf101.colorgrid.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {

    IColorGrid colorGrid;
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    public GridView(IColorGrid colorGrid) {
        this.setPreferredSize(new Dimension(400,300));
        this.colorGrid = colorGrid;

    }

    private void drawGrid(Graphics2D g2){
        double margin = OUTERMARGIN;
        double x = margin;
        double y = margin;
        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin;
        g2.setColor(MARGINCOLOR);
        Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);
        g2.fill(rect);



        CellPositionToPixelConverter pix = new CellPositionToPixelConverter(rect, colorGrid, OUTERMARGIN);
        drawCells(g2,colorGrid, pix);

    }

    private static void drawCells(Graphics2D g2, CellColorCollection c, CellPositionToPixelConverter cellPix){

        for (CellColor cellC: c.getCells()) {
            if(cellC.color() != null){
                g2.setColor(cellC.color());
            }
            else{
                g2.setColor(Color.DARK_GRAY);
            }
            g2.fill(cellPix.getBoundsForCell(cellC.cellPosition()));
        }
    }



    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;


        drawGrid(g2);

    }
}