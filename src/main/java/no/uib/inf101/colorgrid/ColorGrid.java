package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private final int rows;
  private final int cols;
  private final Color[][] grid;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new Color[rows][cols];
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public Color get(CellPosition pos) {
    return this.grid[pos.row()][pos.col()];
  }

  @Override
  public void set(CellPosition pos, Color color) {
    this.grid[pos.row()][pos.col()] = color;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<>();
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        CellPosition pos = new CellPosition(i, j);
        Color color = get(pos);
        cells.add(new CellColor(pos, color));
      }
    }
    return cells;
  }
}